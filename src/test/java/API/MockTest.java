package API;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class MockTest {

    @Test
    public void getPhoneNumberTesting() throws IOException {
        MockWebServer mockServer = new MockWebServer();
        MockResponse mockResponse = new MockResponse()
                .addHeader("Content-Type", "application/json")
                .setResponseCode(200)
                .setBody("{\"phone\": \"+7232434343\"}");
        mockServer.enqueue(mockResponse);
        mockServer.start();

        HttpUrl url = mockServer.url("/getNumber");

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(url).build();
        Response response = client.newCall(request).execute();
        Assertions.assertEquals("{\"phone\": \"+7232434343\"}", response.body().string());
    }

}
