package API.helpers;

import API.models.Category;
import API.models.Pet;
import API.models.Tag;

import java.util.ArrayList;

public class PetDataGenerator {

    public static Pet generatePetsData(){
        Category category = new Category(1, "name");
        String [] urlsPhoto =  {"string"};
        Tag tag = new Tag(1, "someTag");
        ArrayList<Tag> tags = new ArrayList<>();
        tags.add(tag);
        Pet pet = new Pet(11, category, urlsPhoto, "cat Fedor",tags, "available" );
        return pet;
    }
}
