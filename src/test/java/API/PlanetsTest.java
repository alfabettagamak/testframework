package API;

import com.github.fge.jsonschema.cfg.ValidationConfiguration;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import io.restassured.RestAssured;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.ValidatableResponse;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.nio.file.Path;

import static com.github.fge.jsonschema.SchemaVersion.DRAFTV4;

public class PlanetsTest {
    String baseUrl = "https://swapi.dev/api/planets";
    HttpClient client = HttpClient.newHttpClient();

    @ParameterizedTest
    @ValueSource(ints = {1,2,3,4,5,6})
    public void getPlanetsContractTesting(int page) throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder().uri(URI.create(baseUrl  + "?page=" + Integer.toString(page)))
                .build();
        HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
        Assertions.assertEquals(200, response.statusCode());
        JSONObject jsonBody = new JSONObject(response.body().toString());
        Path filePath = Path.of("/Users/administrator/AQA_junior/test_automation/ExampleTestFramework/src/test/resources/schema.json");
        InputStream file = Files.newInputStream(filePath);
        JSONObject schemaJson = new JSONObject(new JSONTokener(file));

        Schema schema = SchemaLoader.load(schemaJson);
        schema.validate(jsonBody);
    }

    @ParameterizedTest
    @ValueSource(ints = {1,2,3,4,5,6,7})
    public void getPlanetsContractRestAssureTesting(int page){
        RestAssured.baseURI = baseUrl;
        JsonSchemaFactory jsonSchemaFactory = JsonSchemaFactory.newBuilder()
                .setValidationConfiguration(ValidationConfiguration.newBuilder()
                        .setDefaultVersion(DRAFTV4).freeze()).freeze();

        RestAssured.given()
                        .param("page", page)
                    .then()
                        .statusCode(200)
                         .body(JsonSchemaValidator.matchesJsonSchemaInClasspath("schema.json")
                                 .using(jsonSchemaFactory));

    }
}