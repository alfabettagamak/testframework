package API.clients;

import java.util.Properties;

import API.models.Village;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;



public class ClientHibernate {
    // https://docs.jboss.org/hibernate/orm/6.2/quickstart/html_single/
    private static SessionFactory sessionJavaConfigFactory;
    private static SessionFactory buildSessionJavaConfigFactory() {
        try {
            Configuration configuration = new Configuration();

            Properties props = new Properties();
            props.put("hibernate.connection.driver_class", "org.postgresql.Driver");
            props.put("hibernate.connection.url", "jdbc:postgresql://localhost:5432/TestBase");
            props.put("hibernate.connection.username", "postgres");
            props.put("hibernate.connection.password", "admin");
            props.put("hibernate.current_session_context_class", "thread");

            configuration.setProperties(props);

            // TODO: add your class
            configuration.addAnnotatedClass(Village.class);

            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
            System.out.println("Hibernate Java Config serviceRegistry created");

            SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);

            return sessionFactory;
        }
        catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }


        public static SessionFactory getSessionJavaConfigFactory() {
            if(sessionJavaConfigFactory == null) sessionJavaConfigFactory = buildSessionJavaConfigFactory();
            return sessionJavaConfigFactory;
    }

}