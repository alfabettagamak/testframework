package API.clients;

import java.sql.*;

public class DbClient {
    //https://jdbc.postgresql.org/documentation/use/

    private static Connection connection;

    static {
        try{
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/TestBase",
                    "postgres",
                    "admin");

        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        }

        public static ResultSet request(String query) throws SQLException {
            Statement statment = connection.createStatement();
            ResultSet result = statment.executeQuery(query);
            result.next();
            return result;
        }
    }

