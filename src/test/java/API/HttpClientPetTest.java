package API;

import API.clients.DbClient;
import com.fasterxml.jackson.databind.ObjectMapper;
import API.models.Pet;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.sql.ResultSet;
import java.sql.SQLException;

import static API.helpers.PetDataGenerator.generatePetsData;

public class HttpClientPetTest {
    String baseUrl = "https://petstore.swagger.io/v2";
    String endpoint = "/pet";

    HttpClient client;

    @BeforeEach
    public void setup(){
        client = HttpClient.newHttpClient();
    }

    @Test
    public void getPetTesting() throws IOException, InterruptedException, SQLException {
        int petId = 1;
        HttpRequest request = HttpRequest.newBuilder().uri(URI.create(baseUrl + endpoint + "/" + Integer.toString(petId)))
                .build();
        HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
        Assertions.assertEquals(200, response.statusCode());
        // ответ в JSONObject (HaspMap = {'key': 'value'})
        JSONObject jsonResponse = new JSONObject(response.body().toString());
        Assertions.assertAll(
                () -> Assertions.assertEquals(petId, jsonResponse.get("id"), "id питомца не соответствует"),
                () -> Assertions.assertEquals("doggie", jsonResponse.get("name")),
                () ->  Assertions.assertEquals("available", jsonResponse.get("status"))
        );
        ResultSet result = DbClient.request("select * from pets where id=" + petId);
        Assertions.assertEquals(result.getString("id"), "jsonResponse.get(\"id\")");
    }

    @Test
    public void postPetsTesting() throws IOException, InterruptedException {
        // Создаем свой класс (параметры могут быть другим классом))
        Pet newPet = generatePetsData();
        ObjectMapper objectMapper = new ObjectMapper();
        // конвертировали наш собственный класс с данными в строку
        String body = objectMapper.writeValueAsString(newPet);  // может быть строкой сразу {"key": value}

        HttpRequest request = HttpRequest.newBuilder().uri(URI.create(baseUrl + endpoint))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(body)).build();
        HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
        // asserts
        Assertions.assertEquals(200, response.statusCode());

        //конвертируем ответ (тело) в класс Pet
        Pet result = objectMapper.readValue(response.body().toString(), Pet.class);
        Assertions.assertEquals(newPet.id, result.id);
    }


}
