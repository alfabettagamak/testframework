package API;

import API.clients.DbClient;
import API.models.Village;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import API.clients.ClientHibernate;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DBTest {

    @Test
    public void getDataFromDbTesting() throws SQLException {
        int villageId = 1;
        ResultSet result = DbClient.request("select * from village where id=" + villageId);
        Assertions.assertEquals(result.getString("name"), "villariba");
    }

    @Test
    public void getDataHibernate(){
        Village village = new Village("someVillage", 300);

        SessionFactory sessionFactory = ClientHibernate.getSessionJavaConfigFactory();
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        // save object in DB
        session.persist(village);
        session.getTransaction().commit();
        sessionFactory.close();
        System.out.println("NEW ID: " + village.getId());
    }
}
