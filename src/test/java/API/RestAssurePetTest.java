package API;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RestAssurePetTest {

    String baseUrl = "https://petstore.swagger.io/v2";
    String endpoint = "/pet";

    @Test
    public void getPetsRestAssuredTesting(){
        Integer petId = 1;
        RestAssured.baseURI = baseUrl;
        ValidatableResponse response = RestAssured
                .given()
                    .get(endpoint + "/" + petId)
                .then()
                .statusCode(200);
        JsonPath body = response.extract().body().jsonPath();
        response.statusCode(200).extract();
        Assertions.assertEquals(petId, body.get("id"));

        // given
        // when
        // than
    }
}
