package API.models;

import jakarta.persistence.*;

@Entity
@Table(name = "village")
public class Village {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "civilians")
    private int civilians;

    public Village(String name, int civilians) {
        this.name = name;
        this.civilians = civilians;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCivilians() {
        return civilians;
    }

    public void setCivilians(int civilians) {
        this.civilians = civilians;
    }
}
