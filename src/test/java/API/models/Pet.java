package API.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class Pet {

    @JsonProperty("id")
    public int id;

    @JsonProperty("category")
    Category category;

    @JsonProperty("photoUrls")
    String[] photoUrls;

    @JsonProperty("name")
    String name;

    @JsonProperty("tags")
    ArrayList<Tag> tags;

    @JsonProperty("status")
    String status;

    public Pet(int id, Category category, String[] photoUrls, String name, ArrayList<Tag> tags, String status) {
        this.id = id;
        this.category = category;
        this.photoUrls = photoUrls;
        this.name = name;
        this.tags = tags;
        this.status = status;
    }

    public Pet() {
        super();
    }
}
