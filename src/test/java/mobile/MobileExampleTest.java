package mobile;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class MobileExampleTest {
    /*
    Расширение экрана
    Разные ОС
    Обновление приложения
    Эффект кармана
    Ориентация
    Сеть - при отсутсвии сети/ потеря сети/ сеть / низкий уровень
    Батерия
    Производительность
    Контрастность
     */

    /*
    node.js
    npm install -g appium
    npm install wd
    Appium -a 127.0.0.1 -p 4723
     */


    private DesiredCapabilities caps;
    private AppiumDriver driver;

    @BeforeEach
    public void BeforeTest(){
        caps = new DesiredCapabilities();
        caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Pixel 2 API 27");
        caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");

    }

    @AfterEach
    public void AfterTest(){
        driver.quit();
    }

    @Test
    void mainTesting() throws InterruptedException, MalformedURLException {

        caps.setCapability(MobileCapabilityType.APP, "/Users/administrator/AQA_junior/test_automation/ExampleTestFramework/src/main/resources/example.apk");
        driver = new AppiumDriver(new URL("http://127.0.0.1:4723/wd/hub"), caps);

        List<WebElement> elementList = driver.findElements(By.xpath("//*"));

        WebElement element2 = null;

        for (WebElement element:
                elementList) {
            System.out.println(element.getLocation());
            System.out.print(element.getText());
            if (element.getText().equals("2")){
                element2 = element;
            }
        }
        element2.click();
        Thread.sleep(6000);

        }
    @Test
    void mainChrome() throws InterruptedException, MalformedURLException {
        // npm install -g appium --chromedriver_version="61"
        // appium --chromedriver_executable path/to/chrome/
        //https://appium.readthedocs.io/en/latest/en/writing-running-appium/web/chromedriver/
        // appium driver install uiautomator2@2.0.1
        caps.setCapability("automationName", "UiAutomator2");
        caps.setCapability(MobileCapabilityType.BROWSER_NAME, "chrome");

        driver = new AppiumDriver(new URL("http://127.0.0.1:4723"), caps);
        driver.get("https://google.com");
        Thread.sleep(6000);
        List<WebElement> elementList = driver.findElements(By.xpath("//a"));
        for (WebElement element:
                elementList) {
            System.out.println(element.getText());
        }



    }
}

