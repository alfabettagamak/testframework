package selenium.helpers;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class FileHelper {

    public static Boolean isEqual(File expectedFile, File actualFile) throws IOException {
        BufferedImage expectedValue = ImageIO.read(expectedFile);
        BufferedImage actualValue = ImageIO.read(actualFile);

        Boolean result = true;
        Color newColor = new Color(136, 0, 61);

        for (int i = 0; i < expectedValue.getWidth(); i++){
            for (int j = 0; j < expectedValue.getHeight(); j++) {
                if (expectedValue.getRGB(i, j) != actualValue.getRGB(i, j)){
                    result = false;
                    actualValue.setRGB(i, j, newColor.getRGB());
                }
            }
        }
        ImageIO.write(actualValue, "png", actualFile);
        return result;
    }
}
