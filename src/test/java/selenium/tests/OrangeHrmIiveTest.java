package selenium.tests;

import io.qameta.allure.*;
import org.junit.Ignore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.*;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;
import selenium.helpers.FileHelper;
import selenium.helpers.ScreenHelper;
import selenium.pages.BasePage;
import selenium.pages.DashboardPage;
import selenium.pages.LoginPage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

@Epic("Front")
@Feature("Главная страница сайта")
//@Env(browser = Env.Browser.chrome)
@ExtendWith(ScreenHelper.class)
public class OrangeHrmIiveTest extends TestBase{

    @Test
    public void checkScreenshot() throws IOException {
        DashboardPage page = new DashboardPage(driver);
        page.open();
        File actualFile = makeScreenshot("actualScreen.png");
        File expectedFile = new File("/Users/administrator/AQA_junior/test_automation/ExampleTestFramework/src/test/java/selenium/screen/expectedScreen.png");
        Assertions.assertTrue(FileHelper.isEqual(expectedFile, actualFile));
    }

    @Test
    public void checkColor() {
        Allure.step("открыть дашборд");
        DashboardPage page = new DashboardPage(driver);
        page.open();
        WebElement element = driver.findElement(By.xpath("//div[@class=\"emp-attendance-chart\"]"));
        String actualColor = element.getCssValue("color");
        Assertions.assertEquals("rgba(0, 0, 0, 1)", actualColor);
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    @Story("Проверка авторизации")
    public void authTesting() {
        Allure.step("открыть дашборд");
        DashboardPage page = new DashboardPage(driver);
        page.open();
        Allure.step("Проверки");
        Assertions.assertTrue(page.getUserLogo().isEnabled());
    }

    @Test
    @Disabled
    public void pageFactoryTest() throws InterruptedException {
        BasePage.menu.admin.click();
        //Thread.sleep(2000);
        BasePage.menu.performance.click();
        //Thread.sleep(2000);
        BasePage.menu.search.sendKeys("ololo");
    }

    @Test
    @Ignore("Не готов фукционал")
    @Link("https://docs.qameta.io/allure/")
    public void cookieTesting() throws InterruptedException {
        ChromeDriver drive2 = new ChromeDriver();
        drive2.get(LoginPage.url);
        drive2.manage().deleteAllCookies();
        drive2.manage().addCookie(BasePage.cookie);
        drive2.get(DashboardPage.url);

       // Thread.sleep(5000);
        drive2.close();
        drive2.quit();
    }

   // @Ignore
    @Test
    public void screenTesting() throws InterruptedException {
        LoginPage page = new LoginPage(driver).open();
        List<WebElement> menu = page.authorization().getMenu();
        Assertions.assertEquals(menu.size(), 11);
        File screen = driver.findElement(By.xpath("//img[@alt=\"profile picture\"]")).getScreenshotAs(OutputType.FILE);
        menu.get(1).click();
        File target = new File("/Users/administrator/AQA_junior/test_automation/ExampleTestFramework/src/test/resources/screen.png");
        screen.renameTo(target);

      //  Thread.sleep(2000);
        TakesScreenshot ts = (TakesScreenshot) driver;
        File screenAll = ts.getScreenshotAs(OutputType.FILE);
        File target1 = new File("/Users/administrator/AQA_junior/test_automation/ExampleTestFramework/src/test/resources/screenAll.png");
        screenAll.renameTo(target1);
       // Thread.sleep(6000);
}

    @Ignore
    @Test
    public void switchTesting() throws InterruptedException {
        new LoginPage(driver).open();
        String mainWindow = driver.getWindowHandle();
        driver.switchTo().newWindow(WindowType.TAB);
        driver.get("https://google.com");
       // Thread.sleep(6000);
        driver.switchTo().window(mainWindow);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("alert('TTTTT!!!!!!!!')");
       // Thread.sleep(3000);
        driver.switchTo().alert().accept();
      //  Thread.sleep(3000);
    }

    @Test
    public void alertTesting() throws InterruptedException {
        new LoginPage(driver).open();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        //js.executeScript("alert('TTTTT!!!!!!!!')");
        // выполнить скрипт js
/*        js.executeScript("document.check = prompt(\"OLOLOLO\")");
        Thread.sleep(3000);
        String expectedText = "FROM TEST!";
        driver.switchTo().alert().sendKeys(expectedText);
        driver.switchTo().alert().accept();
        Thread.sleep(3000);
        // получить значение (результат выполнения скрипта js
        String actualResult = (String) js.executeScript("return document.check");
        Assertions.assertEquals(expectedText, actualResult);*/
        String variable = "HGYYYYYYYYY";
        //document.body.innerHTML = '<h2>HGYYYYYYYYY</h2>'
        // передать свои параметры в скрипт
        js.executeScript("document.body.innerHTML = '<h2>' + arguments[0] + arguments[1] +'</h2>'",
                variable, "!!!!!!");
      //  Thread.sleep(6000);
    }

    @ParameterizedTest
    @CsvSource({"700, 400", "1280, 740"})
    public void windowSizeTesting(int width, int height) throws InterruptedException {
        driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/performance/searchEvaluatePerformanceReview");
        driver.manage().window().setSize(new Dimension(width, height));
        List<WebElement> elements = driver.findElements(By.xpath("//*[contains(text(), 'Select')]"));
        for(WebElement element : elements) {
            Assertions.assertTrue(element.getRect().height > 10);
            Assertions.assertTrue(element.getRect().width > 20);
        }

        JavascriptExecutor js = (JavascriptExecutor) driver;
        Boolean scroll = (Boolean) js.executeScript("return document.documentElement.clientWidth==document.documentElement.scrollWidth");
        Assertions.assertTrue(scroll);
        Thread.sleep(6000);
    }
}
