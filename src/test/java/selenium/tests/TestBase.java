package selenium.tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import selenium.helpers.ScreenHelper;
import selenium.pages.BasePage;
import selenium.pages.LoginPage;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.LinkedHashMap;
import java.util.Map;

public class TestBase implements BeforeEachCallback {
    protected WebDriver driver;
    public static Object data;
    public TakesScreenshot ts;

    @BeforeAll
    public static void beforeClass() throws IOException {
        ChromeDriver driverBefore = new ChromeDriver();
        driverBefore.manage().window().maximize();
        driverBefore.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
        new LoginPage(driverBefore).openAndAuthorize();
        driverBefore.close();
        driverBefore.quit();
       // if (System.getenv("alisa.browser").equals("chrome")) {
        ObjectMapper mapper = new ObjectMapper();
        data = mapper.readValue(new File("/Users/administrator/AQA_junior/test_automation/ExampleTestFramework/src/test/java/selenium/configuration/demo_stand.json"),
                Map.class).get("chrome");
       // }
    }

    @BeforeEach
    public void setup() throws MalformedURLException {
        ChromeOptions options = new ChromeOptions();
//        DesiredCapabilities caps = new DesiredCapabilities();
//        caps.setCapability("enableVideo", true);
       // options.addArguments("--headless=new");
        driver = new ChromeDriver(options);
        //driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), options);
        driver.manage().window().maximize();
        var seconds = ((LinkedHashMap)data).get("wait_seconds");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(Integer.parseInt(seconds.toString())));
        if (BasePage.cookie == null) {
            new LoginPage(driver).open().authorization();
        }
        new LoginPage(driver).open();
        driver.manage().deleteAllCookies();
        driver.manage().addCookie(BasePage.cookie);
        ScreenHelper.driver = driver;
        ts = (TakesScreenshot) driver;
    }

    @AfterEach
    public void tearDown(){
        driver.close();
        driver.quit();
    }

    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
        var setting = context.getRequiredTestClass().getAnnotation(Env.class);
        int a = 6;
    }

    public File makeScreenshot(String fileName){
        File screenAllActual = ts.getScreenshotAs(OutputType.FILE);
        File actualFile = new File("/Users/administrator/AQA_junior/test_automation/ExampleTestFramework/src/test/java/selenium/screen/" + fileName);
        screenAllActual.renameTo(actualFile);
        return actualFile;
    }
}
