package selenium.tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class SeleniumExampleTest {
    public WebDriver driver;

    @BeforeEach
    public void setup(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
        //driver.manage().window().setSize(new Dimension(1800, 1200));

/*        ChromeOptions options = new ChromeOptions();
        options.addArguments()
        driver = new ChromeDriver();*/

    }

    @AfterEach
    public void tearDown(){
        driver.close();
        driver.quit();
    }

    @Test
    public void firstTesting() throws InterruptedException {
        driver.get("https://opensource-demo.orangehrmlive.com/");
        WebElement element = driver.findElement(By.xpath("//input[@name='username']"));
        element.sendKeys("Admin");

/*        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(1));
        wait.until(driver -> driver.findElement(By.xpath("//input[@name='password']")))
                .sendKeys("admin123");*/

        WebElement element1 = driver.findElement(By.xpath("//input[@name='password']"));
        element1.sendKeys("admin123");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        Assertions.assertEquals("https://opensource-demo.orangehrmlive.com/web/index.php/dashboard/index",
                driver.getCurrentUrl());
        WebElement foundElement = driver.findElement(By.xpath("//img[@alt='client brand logo']"));
        Assertions.assertTrue(foundElement.isEnabled());
    //    Thread.sleep(6000);
    }
}
