package selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainMenu {

    WebDriver driver;

    public MainMenu(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//a[@href=\"/web/index.php/time/viewTimeModule\"]" )
    public WebElement time;

    @FindBy(xpath = "//a[@href=\"/web/index.php/admin/viewAdminModule\"]" )
    public WebElement admin;

    @FindBy(xpath = "//a[@href=\"/web/index.php/performance/viewPerformanceModule\"]")
    public WebElement performance;

    @FindBy(xpath = "//input[@placeholder=\"Search\"]")
    public WebElement search;
}
