package selenium.pages;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;

public abstract class BasePage {
    public static MainMenu menu;
    public static Cookie cookie = null;

    protected WebDriver driver;
}
