package selenium.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class DashboardPage extends BasePage{
    public static String url = "https://opensource-demo.orangehrmlive.com/web/index.php/dashboard/index";

    public DashboardPage(WebDriver driver) {
        this.driver = driver;
    }

    @Step("Открыть страницу DashBoard")
    public DashboardPage open(){
        driver.get(url);
        return this;
    }


    public List<WebElement> getMenu(){
        var elements = driver.findElements(
                By.xpath("//ul[@class=\"oxd-main-menu\"]/li"));
        return elements;
    }


    public WebElement getUserLogo(){
        return driver.findElement(By.xpath("//img[@alt='client brand logo']"));
    }

}
