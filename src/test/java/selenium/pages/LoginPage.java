package selenium.pages;

import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage extends BasePage {


    public static String url = "https://opensource-demo.orangehrmlive.com/";

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    @Step("Открыть главную страницу")
    public LoginPage open(){
        driver.get(url);
        return this;
    }

    public DashboardPage authorization(){
        Allure.step("Start auth");
        WebElement element = driver.findElement(By.xpath("//input[@name='username']"));
        element.sendKeys("Admin");
        WebElement element1 = driver.findElement(By.xpath("//input[@name='password']"));
        element1.sendKeys("admin123");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        menu = new MainMenu(driver);
        cookie = driver.manage().getCookieNamed("orangehrm");
        return new DashboardPage(driver);
    }

    public DashboardPage openAndAuthorize(){
        return open().authorization();
    }
}
