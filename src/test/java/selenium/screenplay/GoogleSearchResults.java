package selenium.screenplay;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class GoogleSearchResults implements Question<String> {

    public static Question<Boolean> displayed() {
        return new GoogleSearchResults().asBoolean();
    }

    public String answeredBy(Actor actor) {
        return Text
                .of(GoogleSearchPage.SEARCH_RESULT_TITLES)
                .answeredBy(actor);
    }
}