package org.example;

public class SomeChild extends Some {

    @Override // переопределение
    public void somePrint(){
        System.out.println("SOME Child CLASS");
    }

    // перегрузка
    public void sum(int a, int b){
        int c = a + b ;
    }

    public void sum(double a, double b){
        double c = a + b ;
    }
}
