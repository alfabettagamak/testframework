package org.example;

import java.util.Random;

public class Main {

    // Основы тестирования : черный/ белый ящик,
    // Виды тестирования: функциональные / нефункциональны...
    // пирамида тестирования, тест дизайна (граничные, принятия решений...)
    // тест кейс, чек лист

    // REST, Http... soap (XML)

    // задачки на логику

    // 1. ООП - 4 принципа
    // Абстрактный класс/ метод отличие от интерфейса
    // множественное наследование
    // Static
    // ссылочные и значимые
    // final
    // Object
    // Singleton, Builder, Adapter + PageObject, Page Factory, PageElement ***


    public static void main(String[] args) throws Exception {

        int a = 5;
        int bb = 10;

        a = a + bb;
        bb = a - bb;
        a = a - bb;

        System.out.println(foo());
        Random rnd = new Random();
        int num = rnd.nextInt(100);
        System.out.println(num);
    //    System.out.println(check(num));

        SomeChild someChild = new SomeChild();
        someChild.somePrint();
        int b = 5;
        b = someMethod(b);
        System.out.println(b);
        someChild.sum(2,5);

        // get max just integer
        String str = "fgdf2hgh23hg34hg343hnb435";
        System.out.println("max :" + maxIntFromStr(str));
    }

    public static String foo(){
        Random rnd = new Random();
        int num = rnd.nextInt(101);
        if (num <= 10) return "вероятность 10";
        if (num > 10 && num <= 45) return "вероятность 35";
        if (num > 45 && num <= 100) return "вероятность 55";
        return "";
    }


    public static String check(int num) throws Exception {
        String str = "Кратно";
//        if (num % 3 == 0 && num % 5 == 0){return "Кратно трем и пяти";}
        if (num % 3 == 0) { str = str + " трем"; }
        if (num % 5 == 0) { str = str + " пяти"; }
        if (str.equals("Кратно")) throw new Exception();
        return str;
    }

    public static int maxIntFromStr(String str){
        //String[] strArray = str.split("");
        int max = 0;
        for (char ch: str.toCharArray()) {
            try {
                int a = Integer.parseInt(String.valueOf(ch));
                if (max < a) max = a;
            }
            catch (Exception e){
                continue;
            }
        }
        return max;
    }

    public static int someMethod(int i){
        i++;
        return i;
    }

    // 4! = 1 * 2* 3 * 4
    // 0! = 1
    public int factorial(int number){
        int fact = 1;
        for (int i = 2; i < number; i++) {
            fact = fact * i;
        }
        return fact;
    }

    public int factorial1(int number){
        if (number == 0) return 1;
        return number * factorial1(number - 1);
    }




}